terraform {
  required_providers {
    harbor = {
      source  = "goharbor/harbor"
      version = "3.9.4"
    }
  }
}

variable "harbor_url" {
  type        = string
  description = "URL of the Harbor instance"
  nullable    = false
}

variable "harbor_username" {
  type        = string
  description = "Username for an account with enough privileges to create Harbor projects"
  nullable    = false
}

variable "harbor_password" {
  type        = string
  description = "Password for the account with the username provided in harbor_username"
  nullable    = false
}

variable "app_name" {
  type        = string
  description = "A short name for the application"
  validation {
    condition     = length(var.app_name) >= 3 && length(var.app_name) <= 32 && can(regex("^[a-z][a-z0-9-]+[a-z0-9]$", var.app_name))
    error_message = "The application name must be between 3 and 32 characters in length inclusive, start with an alphabetic character, end with an alphanumeric and contain only those and dashes, and all characters must be lowercase."
  }
  nullable = false
}

variable "domain" {
  type        = string
  description = "The general domain name for STRAP applications"
  nullable    = false
}

variable "webhook_auth_header" {
  type        = string
  description = "The value to use for the Authorization header when calling the webhook"
  nullable    = false
}

provider "harbor" {
  url      = var.harbor_url
  username = var.harbor_username
  password = var.harbor_password
}

# Project for pushing images.
#
# Notes about parameter selections:
# public = false as we are not running a public repository service.  Developers can
#   push to other repositories.
# vulnerability_scanning = true as this is the whole point of using Harbor.
#
# TODO: Things to look at in the future:
# - deployment_security: to prevent images with vulnerabilities with given
#   severity or higher from being deployed
# - storage_quota: storage quota in GB (might be better to use a system
#   policy for this, if that's available)
#
resource "harbor_project" "project" {
  name                   = var.app_name
  public                 = false
  vulnerability_scanning = true
  force_destroy          = true
}

resource "harbor_project_webhook" "scan_completed_webhook" {
  name             = "Scan completed webhook for ${var.app_name}"
  description      = "Webhook to notify when a scan is completed"
  address          = "https://strapper.${var.domain}/webhook/registry/image_high_vulnerabilities/"
  project_id       = harbor_project.project.id
  notify_type      = "http"
  auth_header      = var.webhook_auth_header
  enabled          = true
  skip_cert_verify = true

  events_types = [
    "SCANNING_COMPLETED",
  ]

}

resource "harbor_robot_account" "account" {
  name        = var.app_name
  description = "Robot account for project ${harbor_project.project.name}"
  level       = "project"

  permissions {
    access {
      action   = "push"
      resource = "repository"
    }
    access {
      action   = "pull"
      resource = "repository"
    }

    kind      = "project"
    namespace = harbor_project.project.name
  }
}

output "registry" {
  value = {
    "account" = {
      username = harbor_robot_account.account.name
      password = harbor_robot_account.account.secret
    }
  }
}
